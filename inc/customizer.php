<?php
/**
 * simple Theme Customizer
 *
 * @package simple
 * @since simple 1.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 *
 * @since simple 1.0
 */
function simple_customize_register( $wp_customize ) {

    /* position for site title and description */
	$wp_customize->add_setting( 'simple_header_align', array(
		'default' => 'center',
		'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'simple_header_align', array(
		'type' => 'select',
		'label' => 'Title & Tag Text Position',
		'section' => 'title_tagline',
		'choices' => array(
			'left' => __( 'Left', 'simple' ),
			'center' => __( 'Center', 'simple' ),
			'right' => __( 'Right', 'simple' ),
		),
		'priority' => 3
	) );

	/* Uploader for logo */
    $wp_customize->add_setting( 'simple_logo_img', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw'
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'simple_logo_img', array(
		'label' => __( 'Image/Logo Upload', 'simple' ),
		'section' => 'title_tagline',
		'settings' => 'simple_logo_img'
	) ) );

	/* Logo Corner Settings */
	$wp_customize->add_setting( 'simple_logo_img_style', array(
		'default' => 'round',
		'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'simple_logo_img_style', array(
		'type' => 'select',
		'label' => __( 'Logo/Image Style', 'simple' ),
		'section' => 'title_tagline',
		'choices' => array(
			'round' => __( 'Round', 'simple' ),
			'square' => __( 'Square', 'simple' ),
		),
	) );

    /* Post Formats Navigations */
	$wp_customize->add_setting( 'simple_post_formats_navigation', array(
		'default' => 'hide',
		'sanitize_callback' => 'sanitize_text_field'
	) );

	$wp_customize->add_control( 'simple_post_formats_navigation', array(
		'type' => 'select',
		'label' => __( 'Post Formats Navigation', 'simple' ),
		'section' => 'nav',
		'choices' => array(
			'show' => __( 'Show', 'simple' ),
			'hide' => __( 'Hide', 'simple' ),
		)
	) );

	/**
	* Blog Settings
 	*/
 	$wp_customize->add_section( 'simple_blog_settings', array(
     	'title'    => __( 'Blog Settings', 'simple' ),
     	'priority' => 2000,
	) );

	/* Blog Layout */
	$wp_customize->add_setting( 'simple_blog_date_format', array(
        'default' => 'mdy',
        'sanitize_callback' => 'sanitize_text_field'
    ) );

	$wp_customize->add_control( 'simple_blog_date_format', array(
        'type' => 'select',
        'label' => __( 'Date Format', 'simple' ),
        'section' => 'simple_blog_settings',
        'choices' => array(
            'mdy' => __( 'Month-Day-Year', 'simple' ),
            'dmy' => __( 'Day-Month-Year', 'simple' ),
        ),
        'priority' => 1
    ));
}
add_action( 'customize_register', 'simple_customize_register');
