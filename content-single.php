<?php
/**
 * @package simple
 * @since simple 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h2 class="entry-title"><?php the_title(); ?></h2>

		<?php if ( 'post' == get_post_type() ) : ?>
			<?php simple_post_date(); ?>
		<?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">

		<?php the_content(); ?>

		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'simple' ), 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta bottom">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'simple' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'simple' ) );

			if ( ! simple_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'Esta entrada fue etiquetada %2$s. Guarda el <a href="%3$s" title="Guarda el %4$s" rel="bookmark">enlace</a>.', 'simple' );
				} else {
					$meta_text = __( 'Guarda el <a href="%3$s" title="Guarda el  %4$s" rel="bookmark">enlace</a>.', 'simple' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'Esta entrada fue publicada en %1$s y etiquetada como %2$s. Guarda el <a href="%3$s" title="Permalink to %4$s" rel="bookmark">enlace</a>.', 'simple' );
				} else {
					$meta_text = __( 'Esta entrada fue publicada en %1$s. Guarda el <a href="%3$s" title="Enlace a %4$s" rel="bookmark">enlace</a>.', 'simple' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

		<?php edit_post_link( __( 'Edit', 'simple' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
