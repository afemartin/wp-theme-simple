<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package simple
 * @since simple 1.0
 */
?>

	</div><!-- #main .site-main -->

    <div class='google-adsense container cfmonitor'>
        <ins class="adsbygoogle" data-ad-client="ca-pub-6989343151522642" data-ad-slot="8123026013" data-ad-format="auto"></ins>
    </div>

    <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>

	<div class="footer-container">
		<footer id="colophon" class="site-footer container" role="contentinfo">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<?php if ( ! dynamic_sidebar( 'footer-widget-1' ) ) : ?>
					<?php endif; ?>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<?php if ( ! dynamic_sidebar( 'footer-widget-2' ) ) : ?>
					<?php endif; ?>
				</div>
			</div><!-- .row -->
			<div class="site-info">
				<div class="site-info-inner">
					&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				</div><!-- .site-info-inner -->
			</div><!-- .site-info -->
		</footer><!-- #colophon .site-footer -->
	</div><!-- .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>

</body>
</html>
